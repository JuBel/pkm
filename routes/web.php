<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'proyek@index');

Auth::routes();

Route::post('/register/custom',[
	'uses'=>'signUpController@create',
	'as'=>'register.custom',
]);

Route::post('/login/custom',[
	'uses'=>'loginController@login',
	'as'=>'login.custom',
]);

Route::group(['middleware'=>'auth'],function(){
	Route::get('dashboard',function(){
		return view('dashboard');
	})->name('dashboard');
	Route::get('customer',function(){
		return view('index');
	})->name('customer');
});

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/BuatToko','BuatTokoController');

Route::resource('/DaftarBarang','DaftarBarangController');

Route::resource('/TambahBarang','TambahBarangController');