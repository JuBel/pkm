<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class produk extends Model
{
     protected $fillable=[
    	'nama_produk','deskripsi_produk','gambar','stok','harga','id_toko',
    ];
}
