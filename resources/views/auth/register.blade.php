@extends('layouts.mine')

@section('content')
<center><div class="containerr col-md-4" >
   <!--  <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"> -->
                <div class="white">
               <h2 style="text-align: center" class="mb-3" >{{ __('Register') }}</h2>

               <!--  <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
        
                        <div class="form-group">
                              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Nama" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Enter email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div> -->
           

            <form method="POST" action="{{ route('register.custom') }}">
                {{ csrf_field() }}
                 <div class="form-group">
                    <div class="white">
                        <label for="exampleInputEmail1" >{{ __('Nama') }}</label>
                         <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Nama" value="{{ old('name') }}" required autofocus>


                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Enter email" value="{{ old('email') }}" required autofocus>

                       @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">{{ __('Password') }}</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                </div>
                <div class="form-group">    
                    <label for="exampleInputPassword1">{{ __('Confirm Password') }}</label>
                   <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                </div>

                <div class="form-group">    
                    <label for="exampleInputPassword1">{{ __('Nomor Telepon') }}</label>
                   <input id="Nomor_telepon" type="nomor" class="form-control" name="nomor_telepon" placeholder="Nomor Telepon" required>
                </div>

                <div class="form-group">    
                    <label for="exampleInputPassword1">{{ __('Alamat') }}</label>
                   <input id="alamat" type="alamat" class="form-control" name="Alamat" placeholder="Alamat" required>
                </div>


                <div class="form-group form-check">
                    <div class="col-md-6">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">{{__('Check me out')}}</label>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                    <center><button type="submit" class="btn btn-primary" style="margin-left: 82%">{{ __('Register') }}</button></center>
                </div>
                </div>  
            </form> 
        </div>
    </div>
    </div>
</center>
@endsection
