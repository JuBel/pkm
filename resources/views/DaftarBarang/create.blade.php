@extends('layouts.mine')
  
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">JualBeli</a>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Buat Toko</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Tambah barang</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari barang</button>
    </form>
  </div>
</nav>
  <div class="row">
    @foreach($data_produk as $a)
    <div class="col-md-4">
      <div class="card-deck">
        
        <div class="card">
          <img class="card-img-top" src="{{asset('img/'.$a->gambar)}}" >
          <div class="card-body">
            <h5 class="card-title">{{$a->nama_produk}}</h5>
            <p class="card-text">{{$a->deskripsi_produk}}</p>
          </div>
          <div class="card-body">
          <a href="#" class="card-link">Detail Barang</a>
        </div>
        </div>
        </div>
        </div>
        @endforeach

      
  </div>    
<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <li class="page-item disabled">
      <a class="page-link" href="#" tabindex="-1">Previous</a>
    </li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
      <a class="page-link" href="#">Next</a>
    </li>
  </ul>
</nav>

@endsection