@extends('layouts.mine')

@section('content')
	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">Buat Toko</div>

					<div class="card-body">
							<form action="{{ URL('BuatToko') }} " method="post" enctype="multipart/form-data">
								{{csrf_field()}}
								 <div class="form-group">
								 	 <div class="form-group col-md-6">
								    <label for="formGroupExampleInput">Nama Toko</label>
								    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nama Toko" name="nama_toko">
								  </div>
								</div>
								
								  <div class="form-group">
								  	 <div class="form-group col-md-6">
								    <label for="exampleFormControlFile1">Logo Toko</label>
								    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="gambar_toko" >
								  </div>
								
								 <div class="form-group">
								 	 <div class="form-group col-md-6">
								    <label for="formGroupExampleInput">Lokasi Toko</label>
								    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Lokasi Toko" name="alamat_toko">
								  </div>
								<div class="form-group">
									 <div class="form-group col-md-6">
								    <label for="exampleFormControlTextarea1">Deskripsi Toko</label>
								    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Deskripsi Toko" name="deskripsi"></textarea>
								  </div>
								  <div class="form-group">
								  	<center><button type="submit" class="btn btn-primary" style="margin-left: 82%">{{ __('Submit') }} </button></center>
								  </div>
							  </form>
						</div>
				</div>
			</div>
		</div>
	</div>
@endsection