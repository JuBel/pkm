<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jumlah');
            $table->integer('total_harga');
            $table->dateTime('tanggal_pemesanan');
            $table->dateTime('tanggal_pembayaran');
            $table->string('status');
            $table->integer('id_pengiriman')->unsigned();
            $table->foreign('id_pengiriman')->references('id')->on('metode_pengirimans')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->integer('id_pembayaran')->unsigned();
            $table->foreign('id_pembayaran')->references('id')->on('metode_pembayarans')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->integer('id_produk')->unsigned();
            $table->foreign('id_produk')->references('id')->on('produks')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanans');
    }
}
