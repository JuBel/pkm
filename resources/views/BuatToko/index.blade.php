@extends('layouts.mine')
@section('content')
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">JualBeli</a>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Tambah barang</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari barang</button>
    </form>
  </div>
</nav>
<div class="container bg-light">
        <div class="row">
            <div class="col-md-12">
            	@foreach($data_toko as $data)
				 <center><h3 class="card-title">Toko</h3></center>
				 <table class="table table-responsive">
				 	<tr>
				 		<td>Nama Toko</td>
				 		<td>:</td>
				 		<td>{{ $data->nama_toko }}</td>
				 	</tr>
				 	<tr>
				 		<td>Logo Toko</td>
				 		<td>:</td>
				 		<td><img src="{{ asset('img/'.$data->gambar_toko)}}" width="30px" height="30px"></td>
				 	</tr>
				 	<tr>
				 		<td>Alamat Pemilik</td>
				 		<td>:</td>
				 		<td>{{ $data->alamat_pemilik }}</td>
				 	</tr>
				 	<tr>
				 		<td>Deskripsi Toko</td>
				 		<td>:</td>
				 		<td>{{ $data->deskripsi}}</td>
				 	</tr>
				 	<tr>
				 		<td>Tanggal Pembuatan</td>
				 		<td>:</td>
				 		<td>{{ $data->created_at}}</td>
				 	</tr>
				 </table>
				 @endforeach
			</div>
		</div>
</div>			              	
 <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="card-title">Daftar Barang</h3>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ url('/TambahBarang/create') }}" class="btn btn-primary btn-sm float-right">Tambah Data</a>
                            </div>
                        </div>
                    </div>
		                    <div class="card-body">
		                        @if (session('success'))
		                            <div class="alert alert-success">
		                                {!! session('success') !!}
		                            </div>
		                        @endif
		                        <table class="table table-hover table-bordered">
		                            <thead>
		                                <tr>
		                                    <th>Nama Produk</th>
		                                    <th>Deskripsi</th>
		                                    <th>Gambar</th>
		                                    <th>Stok</th>
		                                    <th>Harga</th>
		                                    <th>Tanggal</th>
		                                    <th>Aksi</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                               
		                                @forelse($produk as $produk)
		                                <tr>
		                                    <!-- MENAMPILKAN VALUE DARI TITLE -->
		                                    <td>{{ $produk->nama_produk }}</td>
		                                    <!-- <td>{{ $produk->nama_produk }}</td>
		                                    <td>{{ $produk->deskripsi }}</td>
		                                    <td>{{ $produk->stok }}</td>
		                                    <td>{{ $produk->stok }}</td> -->
		                                    <td>{{ str_limit($produk->deskripsi_produk, 50) }}</td>
		                                    <td><img src="{{ asset('img/'.$produk->gambar)}}" width="30px" height="30px"></td>
		                                    <td>{{ $produk->stok }}</td>
		                                    <td>Rp {{ number_format($produk->harga) }}</td>
		                                    <td>{{ $produk->created_at->format('d-m-Y') }}</td>
		                                    <!-- TOMBOL DELETE MENGGUNAKAN METHOD DELETE DALAM ROUTING SEHINGGA KITA MEMASUKKAN TOMBOL TERSEBUT KEDALAM TAG <FORM></FORM> -->
		                                    <td>
		                                        <form action="{{ url('/produk/' . $produk->id) }}" method="POST">
		                                            <!-- @csrf ADALAH DIRECTIVE UNTUK MEN-GENERATE TOKEN CSRF -->
		                                            @csrf
		                                            <input type="hidden" name="_method" value="DELETE" class="form-control">
		                                            <a href="{{ url('/produks/' . $produk->id) }}" class="btn btn-warning btn-sm">Edit</a>
		                                            <button class="btn btn-danger btn-sm">Hapus</button>
		                                        </form>
		                                    </td>
		                                </tr>
		                                @empty
		                                <tr>
		                                    <td class="text-center" colspan="6">Tidak ada data</td>
		                                </tr>
		                                @endforelse

		                            </tbody>
		                        </table>
		                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection