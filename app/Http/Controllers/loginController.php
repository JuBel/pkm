<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class loginController extends Controller
{
    public function login(Request $request){
    	if(Auth::attempt(
    		[
    			'email'=>$request->email,
    			'password'=>$request->password,
    		]
    	)){
    		if(User::where('email',$request->email)->first()->is_admin()){
    			return redirect()->route('dashboard');
    		}
    		return redirect()->route('customer');
    	}
        return redirect()->back();
    }
}
