<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuatToko extends Model
{
    protected $fillable=[
    	'nama_toko','gambar_toko','alamat_pemilik','deskripsi','user_id',
    ];
}
