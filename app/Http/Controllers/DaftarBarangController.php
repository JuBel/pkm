<?php

namespace App\Http\Controllers;

use App\DaftarBarang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\produk;

class DaftarBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function create()
    {
        $data_produk = produk::all();
        return view('DaftarBarang.create',compact('data_produk'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DaftarBarang  $daftarBarang
     * @return \Illuminate\Http\Response
     */
    public function show(DaftarBarang $daftarBarang)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DaftarBarang  $daftarBarang
     * @return \Illuminate\Http\Response
     */
    public function edit(DaftarBarang $daftarBarang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DaftarBarang  $daftarBarang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DaftarBarang $daftarBarang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DaftarBarang  $daftarBarang
     * @return \Illuminate\Http\Response
     */
    public function destroy(DaftarBarang $daftarBarang)
    {
        //
    }
}
