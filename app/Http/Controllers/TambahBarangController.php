<?php

namespace App\Http\Controllers;

use App\TambahBarang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\produk;

class TambahBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('TambahBarang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produks = New produk;
        $produks->id_toko = Auth::id();
        $produks->nama_produk = $request->nama_produk;
        // $tokos->gambar-toko = $request->gambar-toko;
        $produks->harga = $request->harga;
        $produks->stok= $request->stok;
        $produks->deskripsi = $request->deskripsi;
        if ($request->gambar) {
            $date=date('dmYHis').'.jpg';
            $request->gambar->storeAs('public/image/barang/','Barang-'.$date);
            $produks->gambar='Barang-'.$date;
        }

        $produks->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TambahBarang  $tambahBarang
     * @return \Illuminate\Http\Response
     */
    public function show(TambahBarang $tambahBarang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TambahBarang  $tambahBarang
     * @return \Illuminate\Http\Response
     */
    public function edit(TambahBarang $tambahBarang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TambahBarang  $tambahBarang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TambahBarang $tambahBarang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TambahBarang  $tambahBarang
     * @return \Illuminate\Http\Response
     */
    public function destroy(TambahBarang $tambahBarang)
    {
        //
    }
}
