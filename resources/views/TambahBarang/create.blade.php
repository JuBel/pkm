@extends('layouts.mine')

@section('content')
	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">Tambah Produk</div>

					<div class="card-body">
							<form action="{{route('TambahBarang.store')}}" method="post" enctype="multipart/form-data">
								{{csrf_field()}}
								 <div class="form-group">
								 	 <div class="form-group col-md-8">
								    <label for="formGroupExampleInput">Nama Produk</label>
								    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nama Produk" name="nama_produk">
								  </div>
								</div>
								
								  <div class="form-group">
								  	 <div class="form-group col-md-8">
								    <label for="exampleFormControlFile1">Gambar Produk</label>
								    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="gambar">
								  </div>
								
								 <div class="form-group">
								 	 <div class="form-group col-md-8">
								    <label for="formGroupExampleInput">Harga Produk</label>
								    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Harga Produk" name="harga">
								  </div>

								<!--  <div class="form-group">
								 	 <div class="form-group col-md-8">
								    <label for="formGroupExampleInput">Potongan Harga</label>
								    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Potongan Harga" >
								  </div> -->
								 <!--  <div class="form-group col-md-8">	
									  <div class="input-group input-group-sm mb-6 col-md-4">
									  	<label for="formGroupExampleInput">Info Tambahan</label>
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="inputGroup-sizing-sm">Small</span>
										  </div>
										  <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
									</div>
									</div>
								</div> -->

								 <div class="form-group">
								 	 <div class="form-group col-md-8">
								    <label for="formGroupExampleInput">Stok</label>
								    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Stok" name="stok">
								  </div>

								<div class="form-group">
									 <div class="form-group col-md-8">
								    <label for="exampleFormControlTextarea1">Deskripsi Produk</label>
								    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Deskripsi Produk" name="deskripsi_produk"></textarea>
								  </div>
								  <div class="form-group">
								  	<center><button type="submit" class="btn btn-primary" style="margin-left: 70%">{{ __('Submit') }} </button></center>
								  </div>
							  </form>
						</div>
				</div>
			</div>
		</div>
	</div>
@endsection