<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\toko;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\produk;
class BuatTokoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = produk::orderBy('created_at', 'DESC')->get(); // 2
        $auth = Auth::id();
        // dd($auth);
        $data_toko = toko::where('user_id', $auth)->get();
        // dd($data_toko->all());
        // CODE DIATAS SAMA DENGAN > select * from `products` order by `created_at` desc 
        return view('BuatToko.index', compact('produk', 'data_toko')); // 3
        // return view('BuatToko.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
         return view('BuatToko.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tokos = New toko;
        $tokos->user_id = Auth::id();
        $tokos->nama_toko = $request->nama_toko;
        // $tokos->gambar-toko = $request->gambar-toko;
        $tokos->alamat_pemilik = $request->alamat_toko;
        $tokos->deskripsi = $request->deskripsi;
        if ($request->gambar_toko) {
            $date=date('dmYHis').'.jpg';
            $request->gambar_toko->storeAs('public/image/toko/','Toko-'.$date);
            $tokos->gambar_toko ='Toko-'.$date;
        }

        $tokos->save();

        return back();
        

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BuatToko  $buatToko
     * @return \Illuminate\Http\Response
     */
    public function show(BuatToko $buatToko)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BuatToko  $buatToko
     * @return \Illuminate\Http\Response
     */
    public function edit(BuatToko $buatToko)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BuatToko  $buatToko
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BuatToko $buatToko)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BuatToko  $buatToko
     * @return \Illuminate\Http\Response
     */
    public function destroy(BuatToko $buatToko)
    {
        //
    }
}
