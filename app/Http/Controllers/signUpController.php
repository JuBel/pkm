<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;

class signUpController extends Controller
{
    public function create(Request $request){
    	//dd($request->Alamat);
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    	$user=User::create([
            'name' => $request->name,
            'email' => $request->email,
            'nomor_telepon'=>$request->nomor_telepon,
            'alamat'=> $request->Alamat,
            'password' => Hash::make($request->password),
        ]);
        return view('auth.login');
    }
}
